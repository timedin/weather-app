
interface Weather {
    precipitation: number;
    day: Date;
    condition: WeatherCondition;
​};
export interface CurrentWeather extends Weather {
    temperature: number;
    windspeed: number;
}
export interface DailyWeather extends Weather {
    maxTemperature: number;
    minTemperature: number;
}
export interface WeatherCondition {
    code: number;
    imgUrl: string;
    description: string;
}