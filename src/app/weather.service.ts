import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { CurrentWeather, DailyWeather, WeatherCondition } from './weather';
import weatherConditions from '../assets/weather-conditions.json'

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private httpClient: HttpClient) {
    this.conditionsMap = new Map(Object.entries(weatherConditions));
  }
  conditionsMap: Map<any,any>;
  timezone: string = "Europe%2FBerlin";
  baseURL:string = "https://api.open-meteo.com/v1/forecast?timezone="+this.timezone+"&";

  getWeatherForecast(currentPosition: GeolocationCoordinates): Observable<DailyWeather[]> {
    var resp = this.httpClient.get<any>(this.getBaseUrl(currentPosition)+"&daily=weather_code,temperature_2m_max,temperature_2m_min,precipitation_sum")
    .pipe<DailyWeather[]>(map((data:any)=>{
      data=data.daily;
      var len:number = (data.time as Array<any>).length;
      var out:DailyWeather[]=[];
      for (let i = 0; i < len; i++) {
        var dailyWeather:DailyWeather={
          maxTemperature: data.temperature_2m_max[i],
          minTemperature: data.temperature_2m_min[i],
          precipitation:  data.precipitation_sum[i],
          day:            new Date(data.time[i]),
          condition:      this.getConditionByID(data.weather_code[i]),
        }
        out.push(dailyWeather);
      }
      return out;
    }));
    return resp;
  }
  getCurrentWeather(currentPosition: GeolocationCoordinates): Observable<CurrentWeather> {
    var resp = this.httpClient.get<any>(this.getBaseUrl(currentPosition)+"&current=temperature_2m,precipitation,weather_code,windspeed")
    .pipe<CurrentWeather>(map((data:any)=>{
      data={
        temperature: data.current.temperature_2m,
        precipitation: data.current.precipitation,
        windspeed: data.current.windspeed,
        condition: this.getConditionByID(data.current.weather_code),
      }
      return data;
    }));
    return resp;
  }
  getConditionByID(id: number):WeatherCondition {
    return {
      code: id,
      imgUrl:this.conditionsMap.get(String(id))?.day.image,
      description: this.conditionsMap.get(String(id))?.day.description,
    }
  }
  getBaseUrl(currentPosition: GeolocationCoordinates) {
    return this.baseURL+"latitude="+currentPosition.latitude+"&longitude="+currentPosition.longitude;
  }
}
