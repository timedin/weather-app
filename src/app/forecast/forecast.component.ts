import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { CurrentWeather, DailyWeather } from '../weather';
import { LocationService } from '../location.service';
@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrl: './forecast.component.css'
})
export class ForecastComponent implements OnInit {
  constructor(private weatherService: WeatherService, private locationService: LocationService) {}
  currentPosition: GeolocationCoordinates = {
    accuracy: 0,
    altitude: null,
    altitudeAccuracy: null,
    heading: null,
    latitude: 52.52,
    longitude: 13.41,
    speed: null
  }
  currentWeather: CurrentWeather = {
    temperature: 0,
    precipitation: 0,
    windspeed: 0,
    day: new Date(),
    condition: {
      code: 0,
      imgUrl: '',
      description: '',
    }
  };
  locationError: boolean = false;

  weatherForecast: DailyWeather[] = [];
  refreshDate: Date = new Date();
  ngOnInit(): void {
    this.refreshData();
  }
  updateLocation(): void {
    this.locationError=false;
    this.locationService.getLocation().subscribe(pos=>{
      this.currentPosition=pos;
      this.refreshData();
    },
    error=>{
      this.locationError=true;
    })
  }
  refreshData(): void {
    this.weatherService.getCurrentWeather(this.currentPosition).subscribe((currentWeather:CurrentWeather)=>{
      this.currentWeather=currentWeather;
    });
    this.weatherService.getWeatherForecast(this.currentPosition).subscribe((weatherForecast:DailyWeather[])=>{
      this.weatherForecast=weatherForecast;
      this.refreshDate=new Date();
    });
  }
}