import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor() { }
  getLocation(): Observable<GeolocationCoordinates>{
    return new Observable<GeolocationCoordinates>(obs=>{
      window.navigator.geolocation.getCurrentPosition(
        success=>{
          console.log(success.coords);
          obs.next(success.coords);
          obs.complete();
        },
        error=>obs.error(error),
        {timeout:5000}
      );
    })
  }
}
